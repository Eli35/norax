const { desktopCapturer, remote } = require('electron');
var robot = require("robotjs");
var ipc = require('electron').ipcRenderer;
import Peer from 'peerjs';
const { Menu } = remote;

/* Peers variables */

var peerKeyboard = new Peer();
var peerMouse = new Peer();
var peerScreen = new Peer();
var peerKeyboardConn = 0;
var peerMouseConn = 0;
var peerScreenConn = 0;

/* */

/* Global variables */

var mouseKey;
var clientXY;
var combinedKeys;
var ids = [0, 0, 0];
var stream = 0;
var code = '';

/* */

/* HTML elements */

  /* Video element */

  const videoElement = document.querySelector('video');

  /* */

  /* Code text */

  const codeText = document.getElementById('codeText');
  codeText.style.display = 'none';
  const currentCode = document.getElementById('currentCode');

  /* */

  /* Select video source button */

  const videoSelectBtn = document.getElementById('videoSelectBtn');
  videoSelectBtn.onclick = getVideoSources;

  /* */

  /* Back to menu button element */

  const backToMenuBtn = document.getElementById('BackToMenu');
  backToMenuBtn.onclick = backToMenu;

  /* */

/* */

/* Open peers */
peerKeyboard.on('open', function (id) {
  console.log('My peer ID is: ' + id);
  ids[1] = id;
});

peerMouse.on('open', function (id) {
  console.log('My peer ID is: ' + id);
  ids[2] = id;
});

peerScreen.on('open', function (id) {
  console.log('My peer ID is: ' + id);
  ids[0] = id;
});

/**/

/* On connection */

peerKeyboard.on('connection', function(conn){
  peerKeyboardConn = conn;
  conn.on('data', (data)=>{
    if (data == 'User connected!') {
      console.log(data + ' ' + conn.peer)
      return;
    }
    else if(data == "Meta")
    {
      data = "command";
    }
    else
    {
      if(data.length > 1 && data.includes("+"))
      {
        combinedKeys = data.split("+");
        robot.keyTap(combinedKeys[0].toLowerCase(), [combinedKeys[1].toLowerCase()]);
      }
      else
      {
        robot.keyTap(data.toLowerCase());
      }
    }
  });
});

peerMouse.on('connection', function (conn) {
  peerMouseConn = conn;
  conn.on('data', (data) => {
    if (data == 'User connected!') {
      console.log(data + ' ' + conn.peer)
      return;
    }
    else if (data == "Meta") {
      data = "command";
    }
    // : for mouse movement XY
    else if (data.includes(":") && data.length > 1) {
      clientXY = data.split(":");
      robot.moveMouse(1920 * clientXY[0], 1080 * clientXY[1]);
    }
    else if (data.includes("m") && data.length > 1) {
      if (data[1] == "0") {
        mouseKey = "left";
      }
      else if (data[1] == "1") {
        mouseKey = "middle";
      }
      else {
        mouseKey = "right";
      }

      if(data.includes("up"))
      {
        robot.mouseToggle("up", mouseKey)
      }
      else
      {
        robot.mouseToggle("down", mouseKey);
      }
    }
  });
});

peerScreen.on('connection', function (conn) {
  peerScreenConn = conn;
  callPeer();
  conn.on('data', (data) => {
    if (data == 'User connected!') {
      console.log(data + ' ' + conn.peer)
      return;
    }
  });
});

/* */

// Send the video stream to the control client
function callPeer() {
  peerScreen.call(peerScreenConn.peer, stream);
  videoElement.srcObject = stream;
  videoElement.play();
}


// Get the available video sources
async function getVideoSources() {
  const inputSources = await desktopCapturer.getSources({
    types: ['screen']
  });

  const videoOptionsMenu = Menu.buildFromTemplate(
    inputSources.map(source => {
      return {
        label: source.name,
        click: () => selectSource(source)
      };
    })
  );


  videoOptionsMenu.popup();
}

// Change the videoSource window to record
async function selectSource(source) {
  const constraints = {
    audio: false,
    video: {
      mandatory: {
        chromeMediaSource: 'desktop',
        chromeMediaSourceId: source.id
      }
    }
  };

  // Create a Stream
  stream = await navigator.mediaDevices
    .getUserMedia(constraints);
  
  code = ids[0] + '|' + ids[1] + '|' + ids[2];
  videoSelectBtn.style.display = 'none';
  codeText.style.display = 'block';
  currentCode.innerText = code;
  videoSelectBtn.innerText = source.name;
}

// Go back to the main menu
function backToMenu() {
  ipc.send('backToMenu');
}