var ipc = require('electron').ipcRenderer;
import Peer from 'peerjs';


/* Peers variables */

var peerKeyboard = new Peer();
var peerMouse = new Peer();
var peerScreen  = new Peer();
var peerKeyboardConn = 0;
var peerMouseConn = 0;
var peerScreenConn = 0;

/* */

/* HTML elements */

  /* Video element */

  const videoElement = document.getElementById('screenshare');
  videoElement.style.display = 'none';

  /* */

  /* Full screen button element*/

  const toggleFullscreenElement = document.getElementById('toggleFullscreen');
  toggleFullscreenElement.style.display = 'none';
  toggleFullscreenElement.onclick = toggleFullScreen;

  /* */

  /* Code input element */

  const codeInputElement = document.getElementById('codeInput');
  const submitButtonElement = document.getElementById('submitBtn');
  submitButtonElement.onclick = connectToClient;
  const codeElement = document.getElementById('code');

  /* */

  /* Back to menu button element */

  const backToMenuBtn = document.getElementById('BackToMenu');
  backToMenuBtn.onclick = backToMenu;

  /* */

/* */

/* Open peers event */

peerScreen.on('open', function (id) {
  console.log('My peer ID is: ' + id);
  
});

peerKeyboard.on('open', function (id) {
  console.log('My peer ID is: ' + id);
  
});

peerMouse.on('open', function (id) {
  console.log('My peer ID is: ' + id);
  
});

/* */

/* Event Listeners */

  document.addEventListener('keydown', (event) => {
    var keyName = event.key;
    if (peerKeyboardConn != 0)
    {
      if (keyName == 'Control' || keyName == 'Shift' || keyName == 'Alt') 
      {
        return;
      }
      else if (keyName == "Meta") 
      {
        keyName = "command";
      }
      else if(event.ctrlKey)
      {
        keyName += "+" + "Control";
      }
      else if(event.shiftKey)
      {      
        keyName += "+" + "Shift";
      }
      else if(event.altKey)
      {
        keyName += "+" + "Alt";
      }
      peerKeyboardConn.send(keyName);
    }
  });

  videoElement.addEventListener('mousemove', (event) => {
    if (peerMouseConn != 0)
    {
      peerMouseConn.send(event.offsetX / videoElement.clientWidth + ":" + event.offsetY / videoElement.clientHeight);
    }
  });

  videoElement.addEventListener('mousedown', event => {
    if (peerMouseConn != 0)
    {
      peerMouseConn.send("m" + event.button); //0=left, 2=right
    }
  });

  videoElement.addEventListener('mouseup', event => {
    if (peerMouseConn != 0)
    { 
      peerMouseConn.send("m" + event.button + "up"); //0=left, 2=right
    }
  });

/* */

peerScreen.on('call', function (call) {
  call.answer();
  call.on('stream', function (remoteStream) {
    videoElement.srcObject = remoteStream;
    videoElement.play();
    toggleFullScreen; //to fix input lag at the start + enter & space
    toggleFullScreen;
  });
  });

function toggleFullScreen() {
  if (!videoElement.fullscreenElement) {
      videoElement.requestFullscreen({ navigationUI: "hide" }).then({}).catch(err => {
        alert(`An error occurred while trying to switch into full-screen mode: ${err.message} (${err.name})`);
      });
  } else {
    if (videoElement.exitFullscreen) {
      videoElement.exitFullscreen();
    }
  }
}

// Connect to the controlled client using peer to peer
function connectToClient()
{
  if(codeElement.value.split('|').length == 3){
    videoElement.style.display = 'block';
    toggleFullscreenElement.style.display = 'block';
    codeInputElement.style.display = 'none';
    peerKeyboardConn = peerKeyboard.connect(codeElement.value.split('|')[1]); //Keyboard peer connect
  peerKeyboardConn.on('open', () => {
    peerKeyboardConn.send('User connected!')
  });

    peerMouseConn = peerMouse.connect(codeElement.value.split('|')[2]); //Mouse peer connect
  peerMouseConn.on('open', () => {
    peerMouseConn.send('User connected!')
  });

  peerScreenConn = peerScreen.connect(codeElement.value.split('|')[0]); //Screen peer connect
  peerScreenConn.on('open', () => {
    peerScreenConn.send('User connected!')
  });
  }
}

// Go back to the main menu
function backToMenu() {
  ipc.send('backToMenu');
}